# File service
Сервис, предоставляет функционал для работы с файлами по средствам CRUD операций. 
Файлы храниться в таблице данных в виде массива байт, имени, временных меток создания и изменения 

## Используемые технологии:
- Maven
- Spring-boot: web, jpa, test
- Liquibase
- PostgreSQL
- Testcontainers
- Checkstyle