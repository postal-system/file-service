package io.codero.fileservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.codero.fileservice.dto.CreateFileDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@TestPropertySource(properties = "spring.autoconfigure.exclude=io.codero.interceptor.context.WebContextAutoConfiguration")
class FileControllerTest extends AbstractControllerTest {

    private static final String URL = "/api/files";

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    private final MockMultipartFile mockMultipartFile = new MockMultipartFile(
            "file",
            "file1.txt",
            MediaType.TEXT_PLAIN_VALUE,
            "Hello, World!".getBytes()
    );

    @Test
    void shouldReturnStatusOk() throws Exception {
        mvc.perform(multipart(URL).file(mockMultipartFile))
                .andExpect(status().isOk());
    }

    @Test
    void shouldReturnUUIDIfUseMultipartFile() throws Exception {
        MvcResult result = mvc.perform(multipart(URL).file(mockMultipartFile))
                .andExpect(status().isOk())
                .andReturn();

        String id = result.getResponse().getContentAsString().replaceAll("^\"|\"$", "");
        String idMatcher = UUID.randomUUID().toString();

        Assertions.assertEquals(id.length(), idMatcher.length());
    }

    @Test
    void shouldReturnUUIDIfUseCreateFileDto() throws Exception {
        CreateFileDto dto = new CreateFileDto("someFile.txt", "content".getBytes());
        String json = objectMapper.writeValueAsString(dto);

        MvcResult result = mvc.perform(post(URL + "/create")
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andReturn();

        String id = result.getResponse().getContentAsString().replaceAll("^\"|\"$", "");
        String idMatcher = UUID.randomUUID().toString();

        Assertions.assertEquals(id.length(), idMatcher.length());

        mvc.perform(get(URL + "/" + id))
                .andExpect(status().isOk())
                .andExpect(header().string("Content-Disposition",
                        "attachment; filename=" + dto.getFileName()))
                .andExpect(content().bytes(dto.getContent()));
    }

    @Test
    void shouldReturnFileById() throws Exception {
        MvcResult postResult = mvc.perform(multipart(URL).file(mockMultipartFile))
                .andExpect(status().isOk())
                .andReturn();

        String strUUID = postResult.getResponse()
                .getContentAsString()
                .replaceAll("^\"|\"$", "");

        UUID id = UUID.fromString(strUUID);

        mvc.perform(get(URL + "/" + id))
                .andExpect(status().isOk())
                .andExpect(header().string("Content-Disposition",
                        "attachment; filename=" + mockMultipartFile.getOriginalFilename()))
                .andExpect(content().string("Hello, World!"));
    }

    @Test
    void shouldReturnExceptionIfFileNotExist() throws Exception {
        UUID id = UUID.randomUUID();
        mvc.perform(get(URL + id))
                .andExpect(status().isNotFound());
    }

    @Test
    void shouldDeleteFileIfFileExist() throws Exception {
        MvcResult postResult = mvc.perform(multipart(URL).file(mockMultipartFile))
                .andExpect(status().isOk())
                .andReturn();

        String strUUID = postResult.getResponse()
                .getContentAsString()
                .replaceAll("^\"|\"$", "");

        UUID id = UUID.fromString(strUUID);

        mvc.perform(delete(URL + "/" + id))
                .andExpect(status().isOk());

        mvc.perform(get(URL + id))
                .andExpect(status().isNotFound());
    }

    @Test
    void shouldReturnExceptionFileIfFileNotExist() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete(URL + "/" + id))
                .andExpect(status().isNotFound());
    }
}