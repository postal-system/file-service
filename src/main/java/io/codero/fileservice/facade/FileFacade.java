package io.codero.fileservice.facade;

import io.codero.fileservice.dto.CreateFileDto;
import io.codero.fileservice.entity.FileEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

public interface FileFacade {
    UUID save(MultipartFile file) throws IOException;
    UUID save(CreateFileDto dto);

    FileEntity findById(UUID uuid) throws IOException;

    void delete(UUID uuid);
}
