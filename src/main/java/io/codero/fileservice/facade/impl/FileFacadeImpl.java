package io.codero.fileservice.facade.impl;

import io.codero.fileservice.dto.CreateFileDto;
import io.codero.fileservice.entity.FileEntity;
import io.codero.fileservice.facade.FileFacade;
import io.codero.fileservice.service.FileService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class FileFacadeImpl implements FileFacade {
    private final FileService service;

    @Override
    public UUID save(MultipartFile multipartFile) throws IOException {
        FileEntity file = FileEntity.builder()
                .fileName(multipartFile.getOriginalFilename())
                .content(multipartFile.getBytes())
                .creationTimestamp(LocalDateTime.now())
                .updateTimestamp(LocalDateTime.now())
                .build();
        return service.save(file);
    }

    @Override
    public UUID save(CreateFileDto dto) {
        FileEntity file = FileEntity.builder()
                .fileName(dto.getFileName())
                .content(dto.getContent())
                .creationTimestamp(LocalDateTime.now())
                .updateTimestamp(LocalDateTime.now())
                .build();
        return service.save(file);
    }

    @Override
    public FileEntity findById(UUID uuid) {
        return service.findById(uuid);
    }

    @Override
    public void delete(UUID uuid) {
        service.delete(uuid);
    }
}
