package io.codero.fileservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.Lob;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateFileDto {
    private String fileName;
    private byte[] content;
}
