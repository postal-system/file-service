package io.codero.fileservice.service.impl;

import io.codero.fileservice.entity.FileEntity;
import io.codero.fileservice.exception.NotFoundException;
import io.codero.fileservice.repository.FileRepository;
import io.codero.fileservice.service.FileService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class FileServiceImpl implements FileService {
    private final FileRepository fileRepository;

    public UUID save(FileEntity file) {
        return fileRepository.save(file).getId();
    }

    @Override
    public FileEntity findById(UUID uuid) {
        return fileRepository.findById(uuid)
                .orElseThrow(() -> new NotFoundException(String.format("File wit id = %s cannot be found", uuid)));
    }

    @Transactional(rollbackFor = {NotFoundException.class})
    @Override
    public void delete(UUID uuid) {
        findById(uuid);
        fileRepository.deleteById(uuid);
    }
}
