package io.codero.fileservice.service;

import io.codero.fileservice.entity.FileEntity;
import io.codero.fileservice.exception.NotFoundException;

import java.io.IOException;
import java.util.UUID;

public interface FileService {
    UUID save(FileEntity file);

    FileEntity findById(UUID uuid) throws NotFoundException;

    void delete(UUID uuid);
}
